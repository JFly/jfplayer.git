QT += core
QT += multimedia multimediawidgets
DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
RC_ICONS = imgs/logo.ico

TARGET = JFPlayer

unix{
LIBS += -L$$PWD/lib/linux-ukylin
LIBS += -lavfilter -lavcodec -lavformat -lswscale -lpostproc -lavutil -lavdevice -lswresample -lx264
LIBS += -lrtmp -lssl -lcrypto -lSDL2main -lSDL2  -llog4cplus
#-ljson
LIBS += -L$$PWD/lib/linux-ukylin/EasyDarwin -leasyrtspclient
#LIBS += -L/usr/lib -lGL -ldl -lpthread -lz  -lasound
LIBS += -L$$PWD/lib/linux-ukylin -lyuv
}

win32{
LIBS += -L$$PWD/lib/win -lavutil -lavformat -lavcodec -lavdevice -lavfilter -lswresample -lswscale
LIBS += -L$$PWD/lib/win/SDL2 -lSDL2 -lSDL2main
LIBS += -L$$PWD/lib/win/ -lyuv
}


INCLUDEPATH += ./include /usr/include/c++

TEMPLATE = app

SOURCES += main.cpp \
    player.cpp \
    closefrm.cpp \
    decodeaudiothread.cpp\
    decodevideothread.cpp\
    decodecontrol.cpp

HEADERS +=player.h \
    closefrm.h \    
    decodeaudiothread.h\
    decodevideothread.h\
    decodecontrol.h


FORMS += \
    closefrm.ui \
    player.ui

RESOURCES += \
    resource.qrc
