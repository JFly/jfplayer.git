﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>

SDL_Window *MainWindow::sdlWindow;
SDL_Renderer *MainWindow::sdlRender;
SDL_Texture *MainWindow::sdlTexture;
SDL_Rect MainWindow::sdlRect;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(playClick()));
    //qRegisterMetaType<AVFrame*>("frame");

    QTimer::singleShot(0, this, SLOT(playClick()));

    //play("rtsp://192.168.1.100/Buick.264");
}

MainWindow::~MainWindow()
{
    if(_decodeControl!=NULL)
        _decodeControl->stop();
    delete ui;
}


void MainWindow::play(QString url){
    //_decodeControl=new DecodeControl(this,(void *)ui->centralwidget->winId(),url,renderFrame);
    //_decodeControl->start();
}

void MainWindow::delay(int msec)
{
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void MainWindow::playClick(){

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER)){
        printf( "Could not initialize SDL - %s\n", SDL_GetError());
    }else{
        ui->centralwidget->setGeometry(0,0,1920,1080);
        sdlWindow=SDL_CreateWindowFrom((void *)ui->centralwidget->winId());
        sdlRender=SDL_CreateRenderer(sdlWindow,-1,0);
        sdlTexture=SDL_CreateTexture(sdlRender,SDL_PIXELFORMAT_IYUV,SDL_TEXTUREACCESS_STREAMING,1920,1080);

        sdlRect.x=0;
        sdlRect.y=0;

        SDL_SetEventFilter(handleEvent,nullptr);
    }
    //play("rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov");
    //play("rtsp://192.168.1.100/1.mkv");
    play("rtsp://192.168.1.100/Buick.264");
    //play("rtmp://192.168.1.191/live/index");
    //play("rtsp://192.168.1.21:554/0");
}

void MainWindow::resizeEvent(QResizeEvent *event){
    int w=this->width();
    int h=this->height();
    //ui->centralwidget->setGeometry(0,0,w,h);

    //SDL_RenderClear(sdlRender);

//        SDL_DestroyTexture(sdlTexture);
//        sdlTexture=SDL_CreateTexture(sdlRender,SDL_PIXELFORMAT_IYUV,SDL_TEXTUREACCESS_TARGET,w,h);

    //_decodeControl->changeSize(this->width(),this->height());

    //handleEvent();

}

void MainWindow::renderFrame(AVFrame *frame){
    int w=frame->width;
    int h=frame->height;
    sdlRect.w=w;
    sdlRect.h=h;

    SDL_UpdateYUVTexture(sdlTexture,&sdlRect,
                         frame->data[0],frame->linesize[0],
            frame->data[1],frame->linesize[1],
            frame->data[2],frame->linesize[2]);

    SDL_RenderClear(sdlRender);
    SDL_RenderCopy(sdlRender,sdlTexture,nullptr,&sdlRect);
    SDL_RenderPresent(sdlRender);

}


int MainWindow::handleEvent(void *userdata, SDL_Event * event){
    if (event->type == SDL_WINDOWEVENT)
    {
        int w=event->window.data1;
        int h=event->window.data2;

        switch (event->window.event)
        {
        case SDL_WINDOWEVENT_RESIZED:
            //                SDL_Log("Window %d resized to %dx%d",
            //                    event->window.windowID, event->window.data1,
            //                    event->window.data2);

//            sdlRect.w=w;
//            sdlRect.h=h*(1920/1080);
//            SDL_RenderClear(sdlRender);

            //SDL_SetWindowSize(sdlWindow,w,h);
            //SDL_RenderSetScale(sdlRender,1,1);

//            SDL_DestroyTexture(sdlTexture);
//            sdlTexture=SDL_CreateTexture(sdlRender,SDL_PIXELFORMAT_IYUV,SDL_TEXTUREACCESS_TARGET,w, h);
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            //                SDL_Log("Window %d size changed to %dx%d",
            //                    event->window.windowID, event->window.data1,
            //                    event->window.data2);
            //SDL_SetWindowSize(sdlWindow, event->window.data1, event->window.data2);
            break;
        default:
            break;
        }
    }

    return  0;
}


