﻿#include "closefrm.h"
#include "ui_closefrm.h"

#include "player.h"

CloseFrm::CloseFrm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CloseFrm)
{
    ui->setupUi(this);

//    this->setWindowFlags(Qt::FramelessWindowHint);//无边框
//    this->setAttribute(Qt::WA_TranslucentBackground);//背景透明

    this->activateWindow();

    connect(ui->closeBtn,SIGNAL(clicked(bool)),this,SLOT(clicked()));
}

void CloseFrm::clicked(){
    this->close();

    Player *player = (Player *)parentWidget();
    if(player != NULL)
    {
        player->close();
    }

}

CloseFrm::~CloseFrm()
{
    delete ui;
}
