#ifndef CLOSEFRM_H
#define CLOSEFRM_H

#include <QWidget>

namespace Ui {
class CloseFrm;
}

class CloseFrm : public QWidget
{
    Q_OBJECT

public:
    explicit CloseFrm(QWidget *parent = 0);
    ~CloseFrm();

private:
    Ui::CloseFrm *ui;

public slots:
    void clicked();
};

#endif // CLOSEFRM_H
