﻿#include "decodeaudiothread.h"
#include "decodecontrol.h"
#include <QWidget>

/*
 *参考内容：
 *https://blog.csdn.net/leixiaohua1020/article/details/38979615
 *https://www.cnblogs.com/WushiShengFei/p/10837959.html
 */

Uint32 DecodeControl::audio_len=0;
Uint8 * DecodeControl::audio_chunk=NULL;
Uint8 * DecodeControl::audio_pos=NULL;

DecodeControl *DecodeControl::_decodeControl;

DecodeControl::DecodeControl(QObject *parent,int w,int h,void * winid,QString url,void(* callback)(uint8_t ** data,int * linesize))
{
    _videoW=w;
    _videoH=h;
    _parent=parent;
    _run=0;
    _url=url;
    _decodeControl=this;
    _winid=winid;
    _frameCallback=callback;
}

void DecodeControl::run(){   
    AVFormatContext    *avFormatCtx;
    int                videoIndex,audioIndex;
    AVCodecParameters    *videoCodecCtx,*audioCodecCtx;
    AVPacket *packet;

    QByteArray urlArray= _url.toLocal8Bit();
    char *filepath =urlArray.data();


    //初始化编解码库
#if(LIBAVCODEC_VERSION_MAJOR<58)
    av_register_all();
#endif
#if(LIBAVCODEC_VERSION_MAJOR<57)
    avcodec_register_all();
#endif

    //初始化网络访问
    //avformat_network_init();
    //创建AVFormatContext对象，与码流相关的结构。
    avFormatCtx = avformat_alloc_context();


    AVDictionary *options = NULL;
    //设置链接超时时间3S
    av_dict_set(&options, "stimeout", std::to_string( 3* 1000).c_str(), 0);
    //设置推流的方式tcp，默认udp。
    av_dict_set(&options, "rtsp_transport",  "tcp", 0);
    //缓冲区
    av_dict_set(&options, "buffer_size", "0", 0);

    av_dict_set(&options,"sync","ext",0);

    //播放速度
    //av_dict_set(&options,"atempo","1.5",0);

    //自动开启线程数
    //av_dict_set(&options, "threads", "auto", 0);
    //QString size="800*600";
    //av_dict_set(&options, "video_size", size.toLatin1().constData(), 0);

    //设置缓存1MB
    avFormatCtx->probesize = 1024 *1024;
    //设置数据获取时间3S
    avFormatCtx->max_analyze_duration = 3 * AV_TIME_BASE;

    int *errorCode=0;

    //初始化avFormatCtx结构
    if (avformat_open_input(&avFormatCtx, filepath, NULL, &options) != 0){
        printf("Couldn't open input stream.\n");
        errorCode+=1;
        renderFrame(NULL,errorCode);
        return ;
    }


    //获取音视频流数据信息
    if (avformat_find_stream_info(avFormatCtx, NULL) < 0){
        errorCode+=1;
        renderFrame(NULL,errorCode);
        printf("Couldn't find stream information.\n");
        return ;
    }

    //音频信号解码
    DecodeAudioThread decodeAudioThread;
    //视频信号解码
    _decodeVideoThread=new DecodeVideoThread(this,_videoW,_videoH,renderFrame);
    videoIndex =_decodeVideoThread->checkVideoInfo(avFormatCtx);
    if (videoIndex!= -1){
        videoCodecCtx=avFormatCtx->streams[videoIndex]->codecpar;

        _run=1;

        _decodeVideoThread->start();


        audioIndex=decodeAudioThread.checkAudioInfo(avFormatCtx);
        if(audioIndex!=-1){
            audioCodecCtx=avFormatCtx->streams[audioIndex]->codecpar;
            decodeAudioThread.start();
        }else{
            printf("没有解析到音频数据\n");
        }
    }else{
        printf("没有解析到视频数据\n");
    }
    packet=av_packet_alloc();

    int errorTimes=0;
    while (_run){
        //av_read_frame读取一帧未解码的数据
        if(av_read_frame(avFormatCtx, packet) >= 0){
            errorTimes=0;
            if (packet->stream_index == videoIndex){
                //视频数据
                _decodeVideoThread->addFrame(packet);
            }else if (packet->stream_index == audioIndex){
                //音频数据
                decodeAudioThread.addFrame(packet);
            }
            av_packet_unref(packet);
        }else{
            //printf("--read frame error--\n ");
            errorTimes++;
            if(errorTimes>500){
                //超时退出
                _run=0;
            }
        }
    }

    //sws_freeContext(img_convert_ctx);
    //av_frame_free(&videoFrameYUV);
    //av_frame_free(&videoFrame);

    _decodeVideoThread->stop();
    decodeAudioThread.stop();
    avformat_close_input(&avFormatCtx);

    errorCode+=1;
    renderFrame(NULL,errorCode);
}

void DecodeControl::stop(){
    _run=0;
}

void DecodeControl::changeSize(int w, int h){
    if(_decodeVideoThread!=NULL)
        _decodeVideoThread->changeSize(w,h);
}

void DecodeControl::sdlAudioInit(void *data,Uint8 *stream,int length){
    SDL_memset(stream,0,length);
    if(audio_len<0)
        return;
    length=(length>audio_len?audio_len:length);

    SDL_MixAudio(stream,audio_pos,length,SDL_MIX_MAXVOLUME);
    audio_pos+=length;
    audio_len-=length;
}

void DecodeControl::renderFrame(uint8_t ** data,int * linesize){
    //emit _decodeControl->SDLRender(frame);
    _decodeControl->_frameCallback(data,linesize);
}
