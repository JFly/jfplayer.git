﻿#ifndef AUDIOCONTROL_H
#define AUDIOCONTROL_H

#include <QThread>
#include <QAudioFormat>
#include <QAudioOutput>
#include <QQueue>
#include <QBuffer>


class AudioControl:public QThread
{
    struct AudioData{
        QByteArray data;
        int length;
    };
public:    
    QQueue<AudioData> _audioQueue;
    AudioControl(int rate,int size,int channel,char* error);

    void run() override;
    void playPCM(const char *data,int length);

private:
    QByteArray  _bytesBuffer;

    int _rate,_size,_channel;

    QAudioFormat _audioFormat;
    QAudioOutput *_audioOutput;
    QIODevice *_outDevice;

    QBuffer _audioBuffer;
};

#endif // AUDIOCONTROL_H
