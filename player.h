﻿#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>
#include <QCoreApplication>
#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QPushButton>

#include "closefrm.h"
#include "decodecontrol.h"
#include <QPaintEngine>

extern "C"{
#include "SDL2/SDL.h"
}

namespace Ui {
class Player;
}

class Player : public QWidget
{
    Q_OBJECT

public:
    explicit Player(QWidget *parent = 0);
    void play( std::string url,bool close);
    ~Player();
    //QPaintEngine* paintEngine() const;


private:
    void init();
    Ui::Player *ui;
    int playCallBack(int type, void *pData, int nSize, void *pAppData);
    void playRtmp(std::string url);
    void playRtsp(std::string url);
    CloseFrm *closeFrm;
    DecodeControl *_decodeControl;

    static std::mutex _resizeLock;
    static void sdlResize();
    static void renderFrame(uint8_t ** data,int * linesize);
    static SDL_Window *sdlWindow;
    static SDL_Renderer *sdlRender;
    static SDL_Texture *sdlTexture;
    static SDL_Rect sdlRect;
    static int handleEvent(void *userdata, SDL_Event * event);

public slots:
    void clicked();

protected:
    //将matlab窗口设置为随着窗口变化而变化
    virtual void resizeEvent(QResizeEvent *event) override;
};


#endif // PLAYER_H
