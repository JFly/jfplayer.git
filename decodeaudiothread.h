﻿#ifndef DECODEAUDIOTHREAD_H
#define DECODEAUDIOTHREAD_H

#include <QThread>
#include <queue>
#include <mutex>

#include <QAudioFormat>
#include <QAudioOutput>
#include <QAudioDeviceInfo>
#include <QBuffer>

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libswresample/swresample.h>

#include "SDL2/SDL.h"
}

class DecodeAudioThread:public QThread
{
    Q_OBJECT

public:
    DecodeAudioThread();
    void run() override;

    int checkAudioInfo(AVFormatContext *avFormatCtx);
    int initPlayer(int sampleRate,int sampleSize,int channels);
    void addFrame(AVPacket *packet);
    void stop();

    static  Uint8  *audioChunk;
        static  Uint32  audioLen;
        static  Uint8  *audioPos;
        static void sdlAudioInit(void *uData,Uint8 *stream,int length);

private:
    std::mutex _audioQueueLock;
    //QQueue<AVPacket*> _audioQueue;
    std::queue<AVPacket*> _audioQueue;

    AVCodecContext  *_audioCodecCtx;
    AVCodecParameters *_avCodecParameters;
    struct SwrContext *_swrctx;
    uint8_t *_outAudioBuffer;
    int _outAudioSize;
    AVFrame *_audioFrame;


    QIODevice *_outDevice;

    int _run;

    void playPCM(const char *data,int length);
    void decode( AVPacket *packet);

     SDL_AudioSpec _sdlAudioSpec;

     int _out_nb_samples;
};
#endif // DECODEAUDIOTHREAD_H
