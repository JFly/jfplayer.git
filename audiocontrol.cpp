﻿#include "audiocontrol.h"

//QFile pcmFile("C:\\Users\\Huhsan\\Desktop\\a.pcm");
AudioControl::AudioControl(int rate,int size,int channel,char* error){
    _rate=rate;
    _size=size;
    _channel=channel;

    //设置采样率
    _audioFormat.setSampleRate(_rate);
    //设置采样大小，8/16位
    _audioFormat.setSampleSize(_size);
    //设置通道数
    _audioFormat.setChannelCount(_channel);
    //设置编码方式
    _audioFormat.setCodec("audio/pcm");
    //设置字节序
    _audioFormat.setByteOrder(QAudioFormat::LittleEndian);
    //设置样本数据类型
    _audioFormat.setSampleType(QAudioFormat::UnSignedInt);

    //获取默认声卡
    QAudioDeviceInfo deviceInfo=QAudioDeviceInfo::defaultOutputDevice();
    if(deviceInfo.isNull()){
        error=QString("没有找到可用声卡").toUtf8().data();
    }
    if(!deviceInfo.isFormatSupported(_audioFormat))
    {
        error=QString("声卡不支持当前配置").toUtf8().data();
    }

    _audioOutput=new QAudioOutput(deviceInfo,_audioFormat);
    _audioOutput->setBufferSize(1024*1000000);
    _outDevice= _audioOutput->start();

    //pcmFile.open(QIODevice::WriteOnly);
    _audioBuffer.open(QBuffer::ReadWrite);
}

void AudioControl::run(){
    //    while (true) {
    //        int length=_audioOutput->bytesFree();

    //        if(length>0){

    //            char *data;
    //            _audioBuffer.read(data,length);

    //            int l= strlen(data);

    //                if(l>0){
    //                    _outDevice->write(data,length);
    //                }
    //        }
    //    }
}

void AudioControl::playPCM(const char *data,int length){  
    int l= strlen(data);

    if(l>0){
        _outDevice->write(data,length);
    }


    //_audioBuffer.write(data);

    //    AudioData audioData;

    //    int l= strlen(data);

    //    if(l>0){
    //       char *data1=(char *)malloc(l);
    //        memcpy(data1,data,l);

    //        audioData.data=data;
    //        audioData.length=length;
    //        _audioQueue.enqueue(audioData);

    //        _outDevice->write(data,length);
    //    }

    //    if(!_audioQueue.isEmpty()){
    //        AudioData audioData1=_audioQueue.dequeue();
    //        if(_audioOutput->bytesFree()<audioData1.length){
    //            printf("audio buffer is full \n");
    //        }else{

    //           // _outDevice->write(audioData1.data,audioData1.length);
    //        }
    //    }
}
