﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QTime>
#include "decodecontrol.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libswresample/swresample.h>

#include "SDL2/SDL.h"
}


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    DecodeControl *_decodeControl;

    static SDL_Window *sdlWindow;
    static SDL_Renderer *sdlRender;
    static SDL_Texture *sdlTexture;
    static SDL_Rect sdlRect;

    void play(QString url);
    void delay(int sec);
    static int handleEvent(void *userdata, SDL_Event * event);
    static void renderFrame(AVFrame *frame);

private slots:
    void playClick();

protected:
    void resizeEvent(QResizeEvent *event);
    //void paintEvent(QPaintEvent *event);


};
#endif // MAINWINDOW_H
