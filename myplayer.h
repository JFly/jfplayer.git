#ifndef MYPLAYER_H
#define MYPLAYER_H

#include <QWidget>

namespace Ui {
class myPlayer;
}

class myPlayer : public QWidget
{
    Q_OBJECT

public:
    explicit myPlayer(QWidget *parent = nullptr);
    ~myPlayer();

private:
    Ui::myPlayer *ui;
};

#endif // MYPLAYER_H
