﻿#include "decodeaudiothread.h"
#include "QDebug"

Uint32 DecodeAudioThread::audioLen=0;
Uint8 * DecodeAudioThread::audioChunk=NULL;
Uint8 * DecodeAudioThread::audioPos=NULL;

DecodeAudioThread::DecodeAudioThread(){

}

int DecodeAudioThread::checkAudioInfo(AVFormatContext *avFormatCtx){
    int audioIndex=av_find_best_stream(avFormatCtx, AVMEDIA_TYPE_AUDIO, -1, -1, nullptr, 0);

    if(audioIndex<0)
        return -1;

    //02 获取音频编解码信息
    _avCodecParameters=avFormatCtx->streams[audioIndex]->codecpar;

    //03 获取解码器
    AVCodec *audioCodec= avcodec_find_decoder(_avCodecParameters->codec_id);

    //04
    _audioCodecCtx= avcodec_alloc_context3(audioCodec);
    if(_audioCodecCtx==NULL){
        printf("could not allocate AVCodecContext.\n");
        return -1;
    }
    avcodec_parameters_to_context(_audioCodecCtx, _avCodecParameters);

    if(avcodec_open2(_audioCodecCtx,audioCodec,NULL)<0){
        printf("Audio Codec not found.\n");
        //avformat_free_context(avFormatCtx);
        _run=0;
    }else{
        //
        int inChannels=_audioCodecCtx->channels;
        int outChannels =AV_CH_LAYOUT_MONO;

        AVSampleFormat inFormat=_audioCodecCtx->sample_fmt;
        AVSampleFormat outFormat=AV_SAMPLE_FMT_S16;

        int inSampleRate=_audioCodecCtx->sample_rate;
        int outSampleRate=_audioCodecCtx->sample_rate;

        _out_nb_samples=1024;

        int inChannelLayout=av_get_channel_layout_nb_channels(inChannels);
        int outChannelLayout=av_get_channel_layout_nb_channels(outChannels);

        _swrctx=swr_alloc();
        _swrctx=swr_alloc_set_opts(_swrctx,
                                   outChannels,
                                   outFormat,
                                   outSampleRate,
                                   inChannels,
                                   inFormat,
                                   inSampleRate,
                                   0, NULL);

        swr_init(_swrctx);

        //05 创建音频帧
        _audioFrame=av_frame_alloc();

        _outAudioSize = av_samples_get_buffer_size(NULL, outChannelLayout, _out_nb_samples,AV_SAMPLE_FMT_S16,1);
        //_outAudioBuffer=(uint8_t *)av_malloc(_outAudioSize);

        _outAudioBuffer=(uint8_t *)av_malloc(192000 *2);

        int result= initPlayer(outSampleRate,16,1);
        _run=(result==0)?0:1;
    }

    return audioIndex;
}

void DecodeAudioThread::sdlAudioInit(void *uData,Uint8 *stream,int length){
    SDL_memset(stream,0, static_cast<size_t>(length));
    if(audioLen<=0)
        return;

    length=(length>audioLen?audioLen:length);

    SDL_MixAudio(stream,audioPos,length,SDL_MIX_MAXVOLUME);

    audioPos+=static_cast<unsigned int>(length);
    audioLen-=static_cast<unsigned int>(length);
}

void DecodeAudioThread::run(){
    while (_run) {
        _audioQueueLock.lock();

        if(!_audioQueue.empty()){
            AVPacket *packet=_audioQueue.front();
            _audioQueue.pop();

            _audioQueueLock.unlock();

            decode(packet);
            av_packet_unref(packet);
        }else{
            _audioQueueLock.unlock();

            //SDL_Delay(10);
            QThread::msleep(10);
        }
    }

    swr_free(&_swrctx);
    avcodec_free_context(&_audioCodecCtx);
}

int DecodeAudioThread::initPlayer(int sampleRate,int sampleSize,int channels){

    char* error;
    int result=1;

#ifdef unix
    SDL_memset(&_sdlAudioSpec, 0, sizeof(_sdlAudioSpec));
    _sdlAudioSpec.freq=sampleRate;
    _sdlAudioSpec.format=AUDIO_S16SYS;
    _sdlAudioSpec.channels=channels;
    _sdlAudioSpec.silence=0;
    _sdlAudioSpec.samples=_out_nb_samples;
    _sdlAudioSpec.userdata=_audioCodecCtx;
    _sdlAudioSpec.userdata = static_cast<void*>(this);
    _sdlAudioSpec.callback=sdlAudioInit;

    if(SDL_OpenAudio(&_sdlAudioSpec,NULL)<0){
        printf("SDL 音频播放开启失败");
        result=0;
    }else{
        //Play

    }
#else
    QAudioFormat audioFormat;
    QAudioOutput *audioOutput;

    //设置采样率
    audioFormat.setSampleRate(sampleRate);
    //设置采样大小，8/16位
    audioFormat.setSampleSize(sampleSize);
    //设置通道数
    audioFormat.setChannelCount(channels);
    //设置编码方式
    audioFormat.setCodec("audio/pcm");
    //设置字节序
    audioFormat.setByteOrder(QAudioFormat::LittleEndian);
    //设置样本数据类型
    audioFormat.setSampleType(QAudioFormat::UnSignedInt);

    //获取默认声卡
    QList<QAudioDeviceInfo> ls= QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    QAudioDeviceInfo deviceInfo=QAudioDeviceInfo::defaultOutputDevice();
    if(deviceInfo.isNull()){
        error=QString("没有找到可用声卡").toUtf8().data();
        //qDebug(error);
        result=0;
        printf(error);
    }
    qDebug() << "Device name: " << deviceInfo.deviceName();

    //    QList<int> a= deviceInfo.supportedSampleRates();
    //    QList<int> b= deviceInfo.supportedSampleSizes();
    //    QList<int> c= deviceInfo.supportedChannelCounts();
    //     QStringList d=deviceInfo.supportedCodecs();
    //     QList<QAudioFormat::SampleType> e=deviceInfo.supportedSampleTypes();
    //     QList<QAudioFormat::Endian>  f=deviceInfo.supportedByteOrders();

    if(!deviceInfo.isFormatSupported(audioFormat))
    {
        //       audioFormat=  deviceInfo.nearestFormat(audioFormat);
        //       QString codec= audioFormat.codec();
        //       int sm= audioFormat.sampleRate();
        //       int c=audioFormat.sampleSize();
        //       int d=audioFormat.channelCount();
        error=QString("声卡不支持当前配置").toUtf8().data();
        result=0;
        printf(error);
    }

    if(result!=0){
        audioOutput=new QAudioOutput(deviceInfo,audioFormat);
        //audioOutput->setBufferSize(1024*1000000);
        _outDevice= audioOutput->start();
    }else{
        _outDevice=NULL;
    }
    //_audioBuffer.open(QBuffer::ReadWrite);
#endif

    return result;
}

void DecodeAudioThread::addFrame(AVPacket *packet){
    if(_run){
        _audioQueueLock.lock();

        if(_audioQueue.size() > 5)
        {
            while (_audioQueue.size()>0) {
                AVPacket *pkt=_audioQueue.front();
                _audioQueue.pop();
                av_packet_unref(pkt);
            }
        }

        AVPacket *copyPacket= av_packet_clone(packet);
        _audioQueue.push(copyPacket);

        _audioQueueLock.unlock();
    }
}

void DecodeAudioThread::decode(AVPacket *packet){
    if(!_run)
        return;

    //解码音频
    //int got_audio;
    //AVFrame *audioFrame=av_frame_alloc();
    //int ret=avcodec_decode_audio4(_audioCodecCtx,audioFrame,&got_audio,packet);

    int ret =0;
    if(packet->size>0)
        ret = avcodec_send_packet(_audioCodecCtx, packet);
    while (ret>=0) {
        ret = avcodec_receive_frame(_audioCodecCtx, _audioFrame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF){
            //printf("Decode audio Error 1.\n");
        }else if (ret < 0) {
            printf("Decode audio Error 2.\n");
        }else{

#ifdef unix
            if(_sdlAudioSpec.samples!=_audioFrame->nb_samples){//MP3、AAC不同的samples，重定位

                SDL_CloseAudio();

                _out_nb_samples = _audioFrame->nb_samples;
                _outAudioSize=av_samples_get_buffer_size(NULL,2,_out_nb_samples,AV_SAMPLE_FMT_S16,1);

                _sdlAudioSpec.samples=_out_nb_samples;
                SDL_OpenAudio(&_sdlAudioSpec,NULL);
            }

            SDL_PauseAudio(0);

#endif

            int len = swr_convert(_swrctx,
                                  &_outAudioBuffer,  19200 ,
                                  (const uint8_t **)_audioFrame->data, _audioFrame->nb_samples);
            if (len>0){
                int size=len*1*2;
                //            int out_buffer_size=audioFrame->nb_samples *
                //                    2 *
                //                    av_get_bytes_per_sample(AV_SAMPLE_FMT_S16);
                playPCM((char*)_outAudioBuffer,size);
            }

            //av_packet_unref(packet);

            //playPCM((char*)audioFrame->data[0],audioFrame->linesize[0]);

            //av_frame_free(&_audioFrame);
        }
    }
}

void DecodeAudioThread::playPCM(const char *data, int length){
#ifdef unix
    audioChunk=(Uint8*)data;
    audioLen=length;
    audioPos=audioChunk;

    while(audioLen>0){
        SDL_Delay(1);
    }
#else
    if(_outDevice!=NULL)
        _outDevice->write(data,length);

#endif
}

void DecodeAudioThread::stop(){
    _run=0;
}
