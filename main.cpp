﻿#include "player.h"

#include <QApplication>
#include <QProcess>
#ifdef __MINGW32__
#undef main /* Prevents SDL from overriding main() */
#endif
#define __STDC_CONSTANT_MACROS

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::string url;
    bool close=false;
    if (argc ==1) {
        //url = std::string("rtmp://192.168.30.17/live/streamAAA");
        url=std::string("rtsp://192.168.1.222:554/0");
        //url = std::string("rtmp://192.168.1.192/live/d65d29d6da054db7af3ebbdc93f940d6");
    } else {
        url = std::string(argv[1]);
        close=( std::string(argv[2])=="0");
    }

#ifdef a


    QString playUrl=QString::fromStdString(url);
    //if(playUrl.toUpper().indexOf("RTSP")==0){
        QString appPath=qApp->applicationDirPath();
        QString command=appPath+"/ffplay.exe";
        QProcess* process=new QProcess();

        QStringList commdList;
        //commdList.push_back("-i");
        commdList.push_back(QString::fromStdString(url));
        //commdList.push_back("-fs");
        commdList.push_back("-nodisp");
        commdList.push_back("-vn");

        commdList.push_back("-fflags");
        commdList.push_back("nobuffer");
        commdList.push_back("-analyzeduration");
        commdList.push_back("1000000");
        //        commdList.push_back("-rtsp_transport");
        //        commdList.push_back("tcp");
        commdList.push_back("-window_title");
        commdList.push_back(" ");
        commdList.push_back("-fast");
        commdList.push_back("-sync");
        commdList.push_back("ext");
        commdList.push_back("-framedrop");

        process->startDetached(command,commdList);
        //return 0;
    //}

#endif

    Player player;
    player.show();
    player.play(url,close);

    return a.exec();
}
