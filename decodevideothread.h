﻿#ifndef DECODEVIDEOTHREAD_H
#define DECODEVIDEOTHREAD_H

#include <QThread>
#include <QQueue>
#include <queue>
#include <mutex>

#include "libyuv.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libswresample/swresample.h>

#include "SDL2/SDL.h"
}

/**
 * @brief 视频解码
 */
class DecodeVideoThread:public QThread
{
    Q_OBJECT

public:    
    DecodeVideoThread(QObject *parent,int w,int h,void(*func)(uint8_t ** data,int * linesize));
    void run() override;

    int checkVideoInfo(AVFormatContext *avFormatCtx);
    void addFrame(AVPacket *packet);
    void changeSize(int w,int h);
    void stop();

private:
    int _videoW,_videoH;
    int _videoIndex=-1;
    AVCodecContext  *_videoCodecCtx;
    AVFrame *_videoFrame;
    AVCodecParameters *_avCodecParameters;
    AVFormatContext *_avFormatCtx;
    uint8_t * _outVideoBuffer;
    int _outVideoBufferSize;
    struct SwsContext * _videoSwsCxt;
    std::mutex _frameQueueLock;

    std::mutex _swscxtLock;
    //QQueue<AVPacket *> _frameQueue;
    std::queue<AVPacket *> _frameQueue;
    void(* _frameCallback)(uint8_t **data,int * linesize);

    /**
     * @brief 执行循环状态
     */
    int _run=-1;

    /**
     * @brief 解码
     * @param frame
     */
    void decode(AVPacket *packet);
};

#endif // DECODEVIDEOCONTROL_H
