﻿#ifndef DECODECONTROL_H
#define DECODECONTROL_H

#include <QThread>
#include <QImage>
#include <functional>
#include "audiocontrol.h"
#include "decodevideothread.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libswresample/swresample.h>

#include "SDL2/SDL.h"
}


class DecodeControl:public QThread
{
    Q_OBJECT

signals:
    void SDLRender(AVFrame *frame);

public:
    DecodeControl(QObject *parent,int w,int h,void * wid,QString url,void(* callback)(uint8_t ** data,int * linesize));
    void changeSize(int w,int h);

    void stop();
    void run() override;

    static  Uint8  *audio_chunk;
    static  Uint32  audio_len;
    static  Uint8  *audio_pos;
    static void sdlAudioInit(void *data,Uint8 *stream,int length);
    static DecodeControl *_decodeControl;

private:
    QString _url;

    std::function<void(QImage)> _videoCallBack;
    int _run;
    int _sampleRate;

    int _videoW,_videoH;

    QObject *_parent;
    void * _winid;
    DecodeVideoThread *_decodeVideoThread;

    AudioControl *_audioControl;

    /**
     * @brief 视频流初始化完成
     */
    int _videoInitFinish=-1;
    void(* _frameCallback)(uint8_t ** data,int * linesize);

    static void renderFrame(uint8_t ** data,int * linesize);




};

#endif // DECODECONTROL_H
