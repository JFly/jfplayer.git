﻿#include "player.h"
#include "ui_player.h"


std::mutex Player::_resizeLock;
SDL_Window *Player::sdlWindow;
SDL_Renderer *Player::sdlRender;
SDL_Texture *Player::sdlTexture;
SDL_Rect Player::sdlRect;

Player::Player(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Player)
{
    ui->setupUi(this);
    init();
}

void Player::init(){
    ui->widget->setUpdatesEnabled(false);
    closeFrm=new CloseFrm(this);
    closeFrm->setParent(this);
    //closeFrm->setWindowFlags(closeFrm->windowFlags()| Qt::Dialog);

    this->setWindowTitle(QString::fromLocal8Bit("流媒体播放器"));
    this->setWindowIcon(QIcon(":/imgs/logo.png"));


    //todo:需要全屏时候取消以下2行代码注释
    //    this->setWindowFlags(Qt::FramelessWindowHint);//无边框
    //    this->showFullScreen();

    //this->setAttribute(Qt::WA_TranslucentBackground);//背景透明

    QCoreApplication::processEvents();
}

void Player::play(std::string url,bool close){

    if(close)
        closeFrm->show();
    else
        closeFrm->close();

    QCoreApplication::processEvents();

    QString playUrl=QString::fromStdString(url);
    playRtsp(url);
}


void Player::playRtsp(std::string url){
    QCoreApplication::processEvents();

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER)){
        printf( "Could not initialize SDL - %s\n", SDL_GetError());
    }else{
        //        QDesktopWidget* desktop = qApp->desktop();
        //        int screenIndex= desktop->primaryScreen();

        //        _w=desktop->screenGeometry(screenIndex).width();
        //        _h=desktop->screenGeometry(screenIndex).height();

        QWidget *widget= ui->widget;
        int w=widget->width();
        int h=widget->height();

        //widget->setGeometry(0,0,w,h);

        //        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        //        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        //        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        sdlWindow=SDL_CreateWindowFrom((const void *)widget->winId());

#ifdef WIN32
        sdlRender=SDL_CreateRenderer(sdlWindow,-1,SDL_RENDERER_SOFTWARE);
#else
        sdlRender=SDL_CreateRenderer(sdlWindow,-1,SDL_RENDERER_SOFTWARE);
        play_station_=nullptr;
#endif
        sdlTexture=SDL_CreateTexture(sdlRender,SDL_PIXELFORMAT_IYUV,SDL_TEXTUREACCESS_STREAMING,w,h);

        //SDL_GLContext glContext = SDL_GL_CreateContext(sdlWindow);

        sdlRect.x=0;
        sdlRect.y=0;
        sdlRect.w=w;
        sdlRect.h=h;

        //SDL_SetEventFilter(handleEvent,nullptr);

        QCoreApplication::processEvents();
        renderFrame(NULL,0);

        QString playUrl=QString::fromStdString(url);
        _decodeControl=new DecodeControl(nullptr,w,h,static_cast<void*>(ui->widget),playUrl,renderFrame);
        _decodeControl->start();

    }
}

void Player::clicked(){
    if(nullptr!=closeFrm) {
        closeFrm->raise();
        closeFrm->update();
        closeFrm->activateWindow();
    }
    ui->widget->update();
}

void Player::renderFrame(uint8_t ** data,int * linesize){
    //SDL_RenderClear(sdlRender);
     sdlResize();

    if(data==NULL){
        SDL_Surface	*pic;
        if(linesize==0)
            pic=SDL_LoadBMP("imgs/loadingsignal.bmp");
        else
            pic = SDL_LoadBMP("imgs/nosignal.bmp");

        if (NULL != pic){
            SDL_Texture *noSignalTexture = SDL_CreateTextureFromSurface(sdlRender, pic);
            SDL_RenderCopy(sdlRender,noSignalTexture,nullptr,&sdlRect);
            SDL_RenderPresent(sdlRender);
        }
    }else{

        int result=0;

        int w=sdlRect.w;
        int h=sdlRect.h;
        uint8_t *outbuf[4];
        outbuf[0] = (uint8_t*)malloc(w*h);
        outbuf[1] =  (uint8_t*)malloc(w*h>>1);
        outbuf[2] =  (uint8_t*)malloc(w*h>>1);
        outbuf[3] = NULL;

        int outlinesize[4] = {w,w/2, w/2, 0};

        int videoWidth=linesize[0];
        int videoHeight=linesize[3];
        //转换yuv分辨率为窗体长宽
        result= libyuv::I420Scale(
                    data[0],linesize[0],data[1],linesize[1],data[2],linesize[2],videoWidth,videoHeight,
                outbuf[0],outlinesize[0],outbuf[1],outlinesize[1],outbuf[2],outlinesize[2],w,h,
                libyuv::FilterMode::kFilterBox);


        if(result>=0){

            //if(SDL_MUSTLOCK(sdlTexture))
            //SDL_LockTexture(sdlTexture,&sdlRect,static_cast<>(&outbuf[0]),outlinesize[1]);

            result=SDL_UpdateYUVTexture(sdlTexture,&sdlRect,
                                        outbuf[0],outlinesize[0],
                    outbuf[1],outlinesize[1],
                    outbuf[2],outlinesize[2]);

            result= SDL_RenderCopy(sdlRender,sdlTexture,nullptr,&sdlRect);
            if(result>=0)
                SDL_RenderPresent(sdlRender);

            free(outbuf[0]);
            free(outbuf[1]);
            free(outbuf[2]);
            free(outbuf[3]);
        }
    }
}


int Player::handleEvent(void *userdata, SDL_Event * event){
    if (event->type == SDL_WINDOWEVENT)
    {
        int w=event->window.data1;
        int h=event->window.data2;

        switch (event->window.event)
        {
        case SDL_WINDOWEVENT_RESIZED:
            //                SDL_Log("Window %d resized to %dx%d",
            //                    event->window.windowID, event->window.data1,
            //                    event->window.data2);

            //            sdlRect.w=w;
            //            sdlRect.h=h*(1920/1080);
            //            SDL_RenderClear(sdlRender);

            //SDL_SetWindowSize(sdlWindow,w,h);
            //SDL_RenderSetScale(sdlRender,1,1);

            //            SDL_DestroyTexture(sdlTexture);
            //            sdlTexture=SDL_CreateTexture(sdlRender,SDL_PIXELFORMAT_IYUV,SDL_TEXTUREACCESS_TARGET,w, h);
            sdlResize();
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            //                SDL_Log("Window %d size changed to %dx%d",
            //                    event->window.windowID, event->window.data1,
            //                    event->window.data2);
            //SDL_SetWindowSize(sdlWindow, event->window.data1, event->window.data2);
            sdlResize();
            break;
        default:
            break;
        }
    }

    return  0;
}


void Player::resizeEvent(QResizeEvent *event)
{
    QSize size=this->size();

    //    if(_decodeControl!=NULL)
    //        _decodeControl->changeSize(_w,_h);

    int width=size.width();
    int height=size.height();

    ui->widget->resize(size);
    ui->widget->lower();
    ui->widget->update();
    int w=50;
    int x=width-w;
    int y=0;

    if(nullptr!=closeFrm) {
        closeFrm->move(x,y);
        closeFrm->raise();
        closeFrm->update();
        closeFrm->activateWindow();
        closeFrm->isTopLevel();
    }

    QCoreApplication::processEvents();

    //linux下加这句sdlwindow窗体尺寸才会变化
    SDL_SetWindowSize(sdlWindow,width,height);

//    int absValue=30;
//    if(abs(width-_w)>absValue||abs(height-_h)>absValue){

//        if(_resizeLock.try_lock()){
//            _w=width;
//            _h=height;

//            if(_w%2==1)
//                _w-=1;

//            sdlResize();

//            _resizeLock.unlock();
//        }
//    }
}

void Player::sdlResize(){
    int w,h;
    SDL_GetWindowSize(sdlWindow, &w, &h);

    if(sdlRect.w!=w||sdlRect.h!=h){
        SDL_DestroyTexture(sdlTexture);
        sdlTexture=SDL_CreateTexture(sdlRender,SDL_PIXELFORMAT_IYUV,SDL_TEXTUREACCESS_STREAMING,w,h);

        //        SDL_Rect viewPort;
        //        SDL_RenderGetViewport(sdlRender, &viewPort);

        sdlRect.w=w;
        sdlRect.h=h;
        SDL_RenderSetViewport(sdlRender, &sdlRect);

        //SDL_SetTextureScaleMode(sdlTexture,SDL_ScaleMode::SDL_ScaleModeBest);
    }
}

Player::~Player()
{
    closeFrm->close();
    ui->widget->disconnect();
#ifdef unix
    if (nullptr != play_station_) {
        play_station_->Close();
    }
    play_station_ = nullptr;
#endif

    if(nullptr!=_decodeControl){
        _decodeControl->stop();
    }

    delete ui;
}
